<?php
	$login="";
	$backurl = null;
	session_start();
	if(isset($_SESSION['username'])) {
		$login = 1;
	}
	if(isset($_GET['photourl']))
	{
		$photourl = $_GET['photourl'];
	}
	else
	{
		$photourl = "test_image2.jpg";
	}
	if(isset($_GET['page']))
	{
		$page = $_GET['page'];
		$backurl = "page.php?page=$page";
	}
	if(isset($_GET['article']))
	{
		$article = $_GET['article'];
		$backurl = "article.php?menu=2&articleid=$article";
	}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>jquery.iviewer test</title>
        <script type="text/javascript" src="jquery.js" ></script>
        <script type="text/javascript" src="jqueryui.js" ></script>
        <script type="text/javascript" src="jquery.mousewheel.min.js" ></script>
        <script type="text/javascript" src="jquery.iviewer.js" ></script>
		
        <script type="text/javascript">
            var $ = jQuery;
            $(document).ready(function(){
                  var iv1 = $("#viewer").iviewer({
                       src: "test_image.jpg",
                       update_on_resize: false,
                       zoom_animation: false,
                       mousewheel: false,
                       onMouseMove: function(ev, coords) { },
                       onStartDrag: function(ev, coords) { return false; }, //this image will not be dragged
                       onDrag: function(ev, coords) { }
                  });

                   $("#in").click(function(){ iv1.iviewer('zoom_by', 1); });
                   $("#out").click(function(){ iv1.iviewer('zoom_by', -1); });
                   $("#fit").click(function(){ iv1.iviewer('fit'); });
                   $("#orig").click(function(){ iv1.iviewer('set_zoom', 100); });
                   $("#update").click(function(){ iv1.iviewer('update_container_info'); });

                  var iv2 = $("#viewer2").iviewer(
                  {
                      src: "<?php echo $photourl; ?>"
                  });

                  $("#chimg").click(function()
                  {
                    iv2.iviewer('loadImage', "test_image.jpg");
                    return false;
                  });

                  var fill = false;
                  $("#fill").click(function()
                  {
                    fill = !fill;
                    iv2.iviewer('fill_container', fill);
                    return false;
                  });
            });
        </script>
        <link rel="stylesheet" href="jquery.iviewer.css" />
        <style>
            .viewer
            {
                width: 90%;
                height: 800px;
                border: 1px solid black;
                position: relative;
				padding-left: 20px;
            }

            .wrapper
            {
                overflow: hidden;
            }
        </style>
		<link rel="stylesheet" href="css/bootstrap.css" />
    </head>
    <body>
        <h1>Photo Viewer</h1>
		
		<?php  
				if($backurl != null){
					echo "<a href = \"$backurl\" class=\"btn btn-primary btn-large\">Back</a>";
					}
		?>
        <!-- wrapper div is needed for opera because it shows scroll bars for reason -->
        <div class="wrapper">
            <div id="viewer2" class="viewer" style="width: 80%;"></div>
        </div>
		&nbsp;
		<div class="col-md-push-5">
				<!-- This whole block must be moved for the logout and login links to work. only outputs links.-->
			<?php
				if($login == 1)
				{
					echo "<a href=\"logout.php\" class=\"btn btn-danger btn-large\">Logout</a>";			
				}
				else
				{			
					echo "<a href=\"login.php\" class=\"btn btn-primary btn-large\">Login</a>";
				}
			?> 
			<!-- end of logout / login block. -->
			<a href="index.php" class="btn btn-primary">Back to Home</a>
		</div>
		<script src="js/bootstrap.js"></script>
    </body>
</html>
