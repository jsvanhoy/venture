<?php
	$search = "";
	$selection= "";
	$login="";
	session_start();
	if(isset($_SESSION['username'])) {
		$login = 1;
	}
		?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Venture News Archive</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <script src="js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.php" class="logo"><b>News Archive</b></a>
            <!--logo end-->
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
					<!-- This whole block must be moved for the logout and login links to work. only outputs links.-->
					<?php
						if($login == 1)
						{
							echo"<li><a class=\"logout\" href=\"login.php\">Logout</a></li>";		
						}
						else
						{
							echo "<li><a href=\"login.php\" class=\"login\">Login</a></li>";
						}
					?> 
                    
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><img src="img/heritage-header-logo-01.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">Heritage Alliance</h5>
              	  <li class="mt">
                      <a href="index.php">
                          <span>Home</span>
                      </a>
                  </li>	
                  <li class="mt">
                      <a href="tags.php">
                          <span>Search Tags/Keywords</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="newspaper.php">
                          <span>Search Newspapers</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="article.php">
                          <span>Search Articles</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="comic.php">
                          <span>Search Comics</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="advertisement.php">
                          <span>Search Advertisements</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="event.php">
                          <span>Search Events</span>
                      </a>
                  </li>
				  <!-- This whole block must be moved for the logout and login links to work. only outputs links.-->
					<?php
						if($login == 1)
						{
							echo"<li class=\"mt\">";
							echo "<a href=\"edit.php\">Add or Change Data</a>";	
							echo "</li>";
						}
					?> 

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

              <div class="row">
                  <div class="col-lg-9 main-chart">
                  <?php
	if(isset($_GET['menu'])){
		if($_GET['menu'] == 1){
					if(isset($_POST['search']) && isset($_POST['selection'])){
						$search = mysql_real_escape_string(htmlentities($_POST['search']));
						$selection = mysql_real_escape_string(htmlentities($_POST['selection']));
					}
					$conn = oci_pconnect('team_venture', 'TVDB3020', 'pythia2.etsu.edu/csdb');
					if (!$conn) {
						$e = oci_error();
						trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR); 
					}
					if(isset($_GET['eventtitle'])){
						$eventtitle = mysql_real_escape_string(htmlentities($_GET['eventtitle']));
						$statement="
											SELECT event.event_id, event.event_title, event.event_description, event_article.article_id
											FROM event 
											JOIN event_article ON event.event_id = event_article.event_id
											JOIN page ON article.article_id = event_article.article_id
											WHERE event.title LIKE '%$eventtitle%'";
					}
					else{
							$statement="
												SELECT event.event_id, event.event_title, event.event_description, event_article.article_id
												FROM event 
												FULL OUTER JOIN event_article ON event.event_id = event_article.event_id 
												FULL OUTER JOIN article ON article.article_id = event_article.article_id
												WHERE $selection LIKE '%$search%' 
												ORDER BY title ASC
												"; 
						}
				$stid = oci_parse($conn, $statement);
						oci_execute($stid);
						echo "<h1>Events</h1>";
						echo "<table class='table table-bordered'>\n";
						echo "<tr>";
							echo "<td>";
								echo "Event Id";
							echo"</td>";
							echo "<td>";
								echo "Event Title";
							echo"</td>";
							echo "<td>";
								echo "Event Description";
							echo"</td>";
							echo "<td>";
								echo "Articles";
							echo"</td>";
						echo"</tr>";
						while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
									$EVENT_ID = $row['EVENT_ID'];
									$EVENT_TITLE=$row['EVENT_TITLE'];
									$EVENT_DESCRIPTION=$row['EVENT_DESCRIPTION'];
									
							echo "<tr>";
									echo "<td>" . ($EVENT_ID !== null ? htmlentities($EVENT_ID, ENT_QUOTES) : "&nbsp;") . "</td>";									
									echo "<td>" . ($EVENT_TITLE !== null ? htmlentities($EVENT_TITLE, ENT_QUOTES) : "&nbsp;") . "</td>";
									echo "<td>" . ($EVENT_DESCRIPTION !== null ? htmlentities($EVENT_DESCRIPTION , ENT_QUOTES) : "&nbsp;") . "</td>";
									if(!$EVENT_ID == NULL){
									echo "<td><a href=\"article.php?menu=2&eventid="  .($EVENT_ID !== null ? htmlentities($EVENT_ID, ENT_QUOTES) : "&nbsp;") ."\">See Articles</a></td>";
									}
							echo "</tr>";
							
							
						}
						echo "</table>\n";
				?>
				<a href="event.php" class="btn btn-primary btn-large">Reset</a>
				
				<?php
		}
				else if($_GET['menu'] == '2'){  
			
				$conn = oci_pconnect('team_venture', 'TVDB3020', 'pythia2.etsu.edu/csdb');
					if (!$conn) {
						$e = oci_error();
						trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR); 
					}
				if(isset($_GET['eventid'])){
						$eventid = mysql_real_escape_string(htmlentities($_GET['eventid']));
						$statement="
									SELECT event.event_id, event.event_title, event.event_description, article.article_id
									FROM event 
									JOIN event_article ON event.event_id = event_article.event_id
									JOIN article ON article.article_id = event_article.article_id
									WHERE event.event_id LIKE '$eventid'
									";
					}
				else if(isset($_GET['pageid']))
				{
					$pageid = mysql_real_escape_string(htmlentities($_GET['pageid']));
					$statement = "
									SELECT event.event_id, event.event_title, event.event_description, article.article_id
									FROM event 
									JOIN event_article ON event.event_id = event_article.event_id
									JOIN article ON article.article_id = event_article.article_id
									WHERE article.article_id LIKE '%$pageid%'
										";
				}
				$stid = oci_parse($conn, $statement);
						oci_execute($stid);
						echo "<h1>Events</h1>";
						echo "<table class='table table-bordered'>\n";
						echo "<tr>";
							echo "<td>";
								echo "Event Id";
							echo"</td>";
							echo "<td>";
								echo "Event Title";
							echo"</td>";
							echo "<td>";
								echo "Event Description";
							echo"</td>";
							echo "<td>";
								echo "Articles";
							echo"</td>";
						echo"</tr>";
						while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
									$EVENT_ID = $row['EVENT_ID'];
									$EVENT_TITLE=$row['EVENT_TITLE'];
									$EVENT_DESCRIPTION=$row['EVENT_DESCRIPTION'];
									
							echo "<tr>";
									echo "<td>" . ($EVENT_ID !== null ? htmlentities($EVENT_ID, ENT_QUOTES) : "&nbsp;") . "</td>";									
									echo "<td>" . ($EVENT_TITLE !== null ? htmlentities($EVENT_TITLE, ENT_QUOTES) : "&nbsp;") . "</td>";
									echo "<td>" . ($EVENT_DESCRIPTION !== null ? htmlentities($EVENT_DESCRIPTION , ENT_QUOTES) : "&nbsp;") . "</td>";
									echo "<td><a href=\"article.php?menu=2&eventid="  .($EVENT_ID !== null ? htmlentities($EVENT_ID, ENT_QUOTES) : "&nbsp;") ."\">See Articles</a></td>";
							echo "</tr>";
							
							
						}
						echo "</table>\n";
					if(isset($_GET['frompage']))
					{
						echo "<a href=\"{$_SERVER['HTTP_REFERER']}\">Back</a>";
						
						
					}
				?>
				<a href="event.php">Reset</a>
				<?php
				
				}
			}
			else {
				?>
				<h1>Search Events</h1>
				<div class="box">
				  <div class="container-1" style="padding-left:5px">
					  <form method="POST" action="event.php?menu=1">
						<input type="search" name="search" id="search" placeholder="Search..." />
						 <select name="selection">
									<option value="event.event_title">Event Title</option>
									<option value="event.header_name">Header Name</option>
						</select>
						&nbsp;
						<input type="submit" name="commit" value="Search">
					  </form>
					  <a href="index.php">Back</a>
				  </div>
				</div>
                  				
                  </div><!-- /col-lg-9 END SECTION MIDDLE -->                   
              </div><!--/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2014 - Team Venture
              <a href="index.php#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>
    
    <script type="text/javascript" src="js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="js/sparkline-chart.js"></script>    
	<script src="js/zabuto_calendar.js"></script>	
	

	
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
  </body>
</html>
	<?php
	}
?>