<?php
	$search = "";
	$selection= "";
	session_start();
	if(!isset($_SESSION['username'])) {
			header("location: login.php");
			exit();
	}else if(isset($_SESSION['username'])) {
		$login = 1;
	}
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Venture News Archive</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="../js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="../lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style-responsive.css" rel="stylesheet">

    <script src="../js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.php" class="logo"><b>News Archive</b></a>
            <!--logo end-->
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
					<!-- This whole block must be moved for the logout and login links to work. only outputs links.-->
					<?php
						if($login == 1)
						{
							echo"<li><a class=\"logout\" href=\"login.php\">Logout</a></li>";		
						}
						else
						{
							echo "<li><a href=\"login.php\" class=\"login\">Login</a></li>";
						}
					?> 
                    
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><img src="../img/heritage-header-logo-01.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">Heritage Alliance</h5>
              	  <li class="mt">
                      <a href="../index.php">
                          <span>Home</span>
                      </a>
                  </li>	
                  <li class="mt">
                      <a href="edittag.php">
                          <span>Edit Tags/Keywords</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editnewspaper.php">
                          <span>Edit Newspapers</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editarticle.php">
                          <span>Edit Articles</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editcomic.php">
                          <span>Edit Comics</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editadvertisement.php">
                          <span>Edit Advertisements</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editevent.php">
                          <span>Edit Events</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editpage.php">
                          <span>Edit Pages</span>
                      </a>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

              <div class="row">
                  <div class="col-lg-9 main-chart">
                  	<?php
	
	if(isset($_GET['menu'])){
		if($_GET['menu'] == 1){
					if(isset($_POST['search']) && isset($_POST['selection'])){
						$search = mysql_real_escape_string(htmlentities($_POST['search']));
						$selection = mysql_real_escape_string(htmlentities($_POST['selection']));
					}
					if(isset($_GET['companyname'])){
						$search = mysql_real_escape_string(htmlentities($_GET['companyname']));
						$selection = "company_name";
					}
					$conn = oci_pconnect('team_venture', 'TVDB3020', 'pythia2.etsu.edu/csdb');
					if (!$conn) {
						$e = oci_error();
						trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR); 
					}
					
					else{
							$statement="SELECT DISTINCT(advertisement.advertisement_id), advertisement.company_name, advertisement.header_name,advertisement.photo_url, page.advertisement_id, newspaper.excession_number, page.page_number,page.page_id
												FROM advertisement
												JOIN page ON page.advertisement_id = advertisement.advertisement_id
												JOIN newspaper ON page.newspaper_id = newspaper.newspaper_id
												WHERE $selection LIKE '%$search%' 
												ORDER BY advertisement.company_name ASC
												"; 
						}
				$stid = oci_parse($conn, $statement);
						oci_execute($stid);
						echo "<h1>Advertisements</h1>";
						echo "<table class='form form-bordered'>\n";
						echo "<tr>";
							echo "<td>";
								echo "Advertisement Id";
							echo"</td>";
							echo "<td>";
								echo "Company Name";
							echo"</td>";
							echo "<td>";
								echo "Header Name";
							echo"</td>";
							echo "<td>";
								echo "Newspaper Excession";
							echo"</td>";
							echo "<td>";
								echo "Page Number";
							echo"</td>";
							echo "<td>";
								echo "Current Photo";
							echo"</td>";
							echo "<td>";
								echo "Upload New Photo";
							echo"</td>";
							echo "<td>";
								echo "Submit";
							echo"</td>";
						echo"</tr>";
						
						echo"<tr>";
							echo"<form class='form form-bordered' method=\"POST\" action=\"editadvertisement.php?menu=3\" enctype=\"multipart/form-data\">";
									echo "<td>New Advertisement</td>";
									echo "<td><input type=\"text\" name=\"companyname\"/></td>";
									echo "<td><input type=\"text\" name=\"headername\" /></td>";
									echo "<td><input type=\"text\" name=\"excessionnumber\" /></td>";	
									echo "<td><input type=\"text\" name=\"pagenumber\" /></td>";
									echo "<td>.</td>";
									echo "<td>Add photos after submitting</td>";
									echo "<td><input name=\"addnew\" type=\"submit\" value=\"Submit\"></td>";
									echo"</form>"; 
						echo"</tr>";
						while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
									$ADVERTISEMENT_ID = $row['ADVERTISEMENT_ID'];
									$COMPANY_NAME=$row['COMPANY_NAME'];
									$HEADER_NAME=$row['HEADER_NAME'];
									$PHOTO_URL=$row['PHOTO_URL'];
									$PAGE_ID=$row['PAGE_ID'];
									$EXCESSION_NUMBER=$row['EXCESSION_NUMBER'];
									$PAGE_NUMBER=$row['PAGE_NUMBER'];
									
							echo "<tr>";
									echo"<form class='form form-bordered' method=\"POST\" action=\"editadvertisement.php?menu=2\" enctype=\"multipart/form-data\">";
									echo "<td>" . ($ADVERTISEMENT_ID !== null ? htmlentities($ADVERTISEMENT_ID, ENT_QUOTES) : "&nbsp;") . "</td>";
									echo "<td><input type=\"text\" name=\"companyname\" value=\""  . ($COMPANY_NAME !== null ? htmlentities($COMPANY_NAME, ENT_QUOTES) : "&nbsp;") . "\"/></td>";
									echo "<td><input type=\"text\" name=\"headername\" value=\""  .($HEADER_NAME !== null ? htmlentities($HEADER_NAME, ENT_QUOTES) : "&nbsp;") ."\"></td>";
									echo "<td><input type=\"text\" name=\"excessionnumber\" value=\""  .($EXCESSION_NUMBER !== null ? htmlentities($EXCESSION_NUMBER, ENT_QUOTES) : "&nbsp;") ."\"></td>";	
									echo "<td><input type=\"text\" name=\"pagenumber\" value=\""  .($PAGE_NUMBER !== null ? htmlentities($PAGE_NUMBER, ENT_QUOTES) : "&nbsp;") ."\"></td>";
									echo "<td><a href=\"../viewer.php?photourl="  .($PHOTO_URL !== null ? htmlentities($PHOTO_URL, ENT_QUOTES) : "&nbsp;") ."\">See Photo</a></td>";
									echo "<td><input type=\"file\" name=\"photourl\" id=\"file\"/></td>";
									echo "<input type=\"hidden\" name=\"advertisementid\" value=\"" . $ADVERTISEMENT_ID . "\">";
									echo "<td><input name=\"$ADVERTISEMENT_ID\" type=\"submit\" value=\"Submit\"></td>";
									echo"</form>"; 
							echo "</tr>";
							
						}
						echo "</table>\n";
				?>
				<a href="editadvertisement.php">Reset</a>
				
				<?php
		
		}
				else if($_GET['menu'] == '2'){
				$conn = oci_pconnect('team_venture', 'TVDB3020', 'pythia2.etsu.edu/csdb');
					if (!$conn) {
						$e = oci_error();
						trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR); 
					}
						$advertisementid = str_replace(' ', '', mysql_real_escape_string(htmlentities($_POST['advertisementid'])));
						$companyname = mysql_real_escape_string(htmlentities($_POST['companyname']));
						$headername = mysql_real_escape_string(htmlentities($_POST['headername']));
						$excessionnumber = mysql_real_escape_string(htmlentities($_POST['excessionnumber']));
						$pagenumber = mysql_real_escape_string(htmlentities($_POST['pagenumber']));
						
						if(isset( $_FILES['photourl']['name'])){
						$allowed_filetypes = array('.jpg','.jpeg','.png','.gif');
						$max_filesize = 10485760;
						$upload_path = '../photos/';

						$filename1 = $_FILES['photourl']['name'];
						$photourl = "./photos/" . $filename1;
						$ext = substr($filename1, strpos($filename1,'.'), strlen($filename1)-1);

						if(filesize($_FILES['photourl']['tmp_name']) > $max_filesize)
						  die('The file you attempted to upload is too large.');

						if(!is_writable($upload_path))
						  die('You cannot upload to the specified directory, please CHMOD it to 777.');

						if(move_uploaded_file($_FILES['photourl']['tmp_name'],$upload_path . $filename1)) {
						echo 'Your file upload was successful!';
						}
						else{
							echo "file upload failed";
						}
						}
						if($photourl == "./photos/")
						{
							$photourl = null;
						}
						$statement = "
												SELECT * FROM newspaper WHERE excession_number = '$excessionnumber'
											";
						$stid = oci_parse($conn, $statement);
						oci_execute($stid);
						while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
							$newspaperid = $row['NEWSPAPER_ID'];
						}
						$statement = "
												SELECT * FROM page WHERE newspaper_id = '$newspaperid' AND page_number = '$pagenumber'
											";
						$stid = oci_parse($conn, $statement);
						oci_execute($stid);
						$pageid = null;
						while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
							$pageid = $row['PAGE_ID'];
						}
						if($pageid == null){
							echo "<br/><br/><br/><h1> Error : You selected a page and excession number that were incorrect! <br/> Try again with an excession and page number that exists!</h1>";
							echo "<br/><a href=\"editadvertisement.php\" class=\"btn btn-primary btn-large\">Reset</a>";
							exit();
						}
						
						$statement="
										UPDATE advertisement
										SET  company_name = '$companyname',
												header_name = '$headername',
												photo_url = '$photourl'
										WHERE ADVERTISEMENT_ID = '$advertisementid'
										";
						$stid = oci_parse($conn, $statement);
						oci_execute($stid);
						
					
						echo "
						
							<script type=\"text/javascript\">
								<!--
									window.location = \"tagadvertisementlink.php?advertisementid=$advertisementid&menu=1\"
									//-->
								</script>						
								";

					

				?>
				<a href="editadvertisement.php">Reset</a>
				<?php
				
				}
				else if($_GET['menu'] == 3 && isset($_POST['companyname'])){
					
					$conn = oci_pconnect('team_venture', 'TVDB3020', 'pythia2.etsu.edu/csdb');
					if (!$conn) {
						$e = oci_error();
						trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR); 
					}
						$companyname = mysql_real_escape_string(htmlentities($_POST['companyname']));
						$headername = mysql_real_escape_string(htmlentities($_POST['companyname']));
						
					
					$statement3="
												SELECT MAX(advertisement_id) as maximumx  FROM advertisement
									";
													$stid3 = oci_parse($conn, $statement3);
													oci_execute($stid3);
													while ($row = oci_fetch_array($stid3, OCI_ASSOC+OCI_RETURN_NULLS)) 
													{
														$advertisementid = $row['MAXIMUMX'];
													}
													$advertisementid +=1;
					$statement4="
												INSERT INTO advertisement
												( advertisement_id,company_name,header_name,photo_url)
												VALUES
												('$advertisementid', '$companyname', '$headername', null)
									";
													$stid4 = oci_parse($conn, $statement4);
													oci_execute($stid4);
										
											echo "
													<script type=\"text/javascript\">
								<!--
									window.location = \"editadvertisement.php?companyname=$companyname&menu=1\"
									//-->
								</script>						
								";

				
				}
				
			}
			else {
				?>
				<h1>Edit Advertisements</h1>
				<div class="box">
				  <div class="container-1" style="5px">
					  <form method="POST" action="editadvertisement.php?menu=1">
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
						<input type="search" name="search" id="search" placeholder="Search..." />
						 <select name="selection">
									<option value="advertisement.company_name">Company Name</option>
									<option value="advertisement.header_name">Header Name</option>
						</select>
						&nbsp;
						<input type="submit" name="commit" value="Search">
					  </form>
					  <a href="../edit.php">Back</a>
				  </div>
				</div>
                  				
                  </div><!-- /col-lg-9 END SECTION MIDDLE -->                   
              </div><!--/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2014 - Team Venture
              <a href="index.php#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../js/jquery.js"></script>
    <script src="../js/jquery-1.8.3.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../js/jquery.scrollTo.min.js"></script>
    <script src="../js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="../js/common-scripts.js"></script>
    
    <script type="text/javascript" src="../js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="../js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="../js/sparkline-chart.js"></script>    
	<script src="../js/zabuto_calendar.js"></script>	
	

	
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
  </body>
</html>
	<?php
	}
?>