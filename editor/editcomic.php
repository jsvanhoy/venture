<?php
	$search = "";
	$selection= "";
	session_start();
	if(!isset($_SESSION['username'])) {
			header("location: login.php");
			exit();
	}else if(isset($_SESSION['username'])) {
		$login = 1;
	}
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Venture News Archive</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="../js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="../lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style-responsive.css" rel="stylesheet">

    <script src="../js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.php" class="logo"><b>News Archive</b></a>
            <!--logo end-->
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
					<!-- This whole block must be moved for the logout and login links to work. only outputs links.-->
					<?php
						if($login == 1)
						{
							echo"<li><a class=\"logout\" href=\"login.php\">Logout</a></li>";		
						}
						else
						{
							echo "<li><a href=\"login.php\" class=\"login\">Login</a></li>";
						}
					?> 
                    
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><img src="../img/heritage-header-logo-01.png" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">Heritage Alliance</h5>
              	  <li class="mt">
                      <a href="../index.php">
                          <span>Home</span>
                      </a>
                  </li>	
                  <li class="mt">
                      <a href="edittag.php">
                          <span>Edit Tags/Keywords</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editnewspaper.php">
                          <span>Edit Newspapers</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editarticle.php">
                          <span>Edit Articles</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editcomic.php">
                          <span>Edit Comics</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editadvertisement.php">
                          <span>Edit Advertisements</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editevent.php">
                          <span>Edit Events</span>
                      </a>
                  </li>
				  <li class="mt">
                      <a href="editpage.php">
                          <span>Edit Pages</span>
                      </a>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

              <div class="row">
                  <div class="col-lg-9 main-chart">
                  	<?php
	
	if(isset($_GET['menu'])){
		if($_GET['menu'] == 1){
					if(isset($_POST['search']) && isset($_POST['selection'])){
						$search = mysql_real_escape_string(htmlentities($_POST['search']));
						$selection = mysql_real_escape_string(htmlentities($_POST['selection']));
					}
					$conn = oci_pconnect('team_venture', 'TVDB3020', 'pythia2.etsu.edu/csdb');
					if (!$conn) {
						$e = oci_error();
						trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR); 
					}
					if(isset($_GET['authorfirst'])){
						$authorfirst = mysql_real_escape_string(htmlentities($_GET['authorfirst']));
						$statement="SELECT * FROM comic WHERE author_first LIKE '%$authorfirst%'";
					}
					else if(isset($_GET['authorlast'])){
						$authorlast = mysql_real_escape_string(htmlentities($_GET['authorlast']));
						$statement="SELECT * FROM comic WHERE author_last LIKE '%$authorlast%'";
					}
					else{
							$statement="SELECT DISTINCT(comic.comic_id), comic.comic_type, comic.title,comic.author_last, comic.author_first,comic.photo_url, comic.page_id, page.page_number, newspaper.excession_number
												FROM comic
												JOIN page ON page.page_id = comic.page_id
												JOIN newspaper ON page.newspaper_id = newspaper.newspaper_id
												WHERE $selection LIKE '%$search%' 
												ORDER BY comic.title ASC
												"; 
						}
				$stid = oci_parse($conn, $statement);
						oci_execute($stid);
						echo "<h1>Comics</h1>";
						echo "<table class='form form-bordered'>\n";
						echo "<tr>";
							echo "<td>";
								echo "Comic Id";
							echo"</td>";
							echo "<td>";
								echo "Title";
							echo"</td>";
							echo "<td>";
								echo "Author Last Name";
							echo"</td>";
							echo "<td>";
								echo "Author First Name";
							echo"</td>";
							echo "<td>";
								echo "Newspaper Excession";
							echo"</td>";
							echo "<td>";
								echo "Page Number";
							echo"</td>";
							echo "<td>";
								echo "Comic Type";
							echo"</td>";
							echo "<td>";
								echo "Current Photo";
							echo"</td>";
							echo "<td>";
								echo "Upload New Photo";
							echo"</td>";
							echo "<td>";
								echo "Submit";
							echo"</td>";
						echo"</tr>";
						while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
									$COMIC_ID = $row['COMIC_ID'];
									$TITLE=$row['TITLE'];
									$AUTHOR_LAST=$row['AUTHOR_LAST'];
									$AUTHOR_FIRST=$row['AUTHOR_FIRST'];
									$PHOTO_URL=$row['PHOTO_URL'];
									$PAGE_ID=$row['PAGE_ID'];
									$EXCESSION_NUMBER=$row['EXCESSION_NUMBER'];
									$PAGE_NUMBER=$row['PAGE_NUMBER'];
									$COMIC_TYPE = $row['COMIC_TYPE'];
									
							echo "<tr>";
									echo"<form class='form form-bordered' method=\"POST\" action=\"editcomic.php?menu=2\" enctype=\"multipart/form-data\">";
									echo "<td>" . ($COMIC_ID !== null ? htmlentities($COMIC_ID, ENT_QUOTES) : "&nbsp;") . "</td>";
									echo "<td><input type=\"text\" name=\"title\" value=\""  . ($TITLE !== null ? htmlentities($TITLE, ENT_QUOTES) : "&nbsp;") . "\"/></td>";
									echo "<td><input type=\"text\" name=\"authorlast\" value=\""  .($AUTHOR_LAST !== null ? htmlentities($AUTHOR_LAST, ENT_QUOTES) : "&nbsp;") ."\"></td>";
									echo "<td><input type=\"text\" name=\"authorfirst\" value=\""  .($AUTHOR_FIRST !== null ? htmlentities($AUTHOR_FIRST, ENT_QUOTES) : "&nbsp;") ."\"></td>";	
									echo "<td><input type=\"text\" name=\"excessionnumber\" value=\""  .($EXCESSION_NUMBER !== null ? htmlentities($EXCESSION_NUMBER, ENT_QUOTES) : "&nbsp;") ."\"></td>";	
									echo "<td><input type=\"text\" name=\"pagenumber\" value=\""  .($PAGE_NUMBER !== null ? htmlentities($PAGE_NUMBER, ENT_QUOTES) : "&nbsp;") ."\"></td>";
									echo "<td><input type=\"text\" name=\"comictype\" value=\""  .($COMIC_TYPE !== null ? htmlentities($COMIC_TYPE, ENT_QUOTES) : "&nbsp;") ."\"></td>";
									echo "<td><a href=\"../viewer.php?photourl="  .($PHOTO_URL !== null ? htmlentities($PHOTO_URL, ENT_QUOTES) : "&nbsp;") ."\">See Photo</a></td>";
									echo "<td><input type=\"file\" name=\"photourl\" id=\"file\"/></td>";
									echo "<input type=\"hidden\" name=\"comicid\" value=\"" . $COMIC_ID . "\">";
									echo "<td><input name=\"$COMIC_ID\" type=\"submit\" value=\"Submit\"></td>";
									echo"</form>"; 
							echo "</tr>";
							
						}
						echo "</table>\n";
				?>
				<a href="editcomic.php" class="btn btn-primary btn-large">Reset</a>
				
				<?php
		}
				else if($_GET['menu'] == '2'){
				$conn = oci_pconnect('team_venture', 'TVDB3020', 'pythia2.etsu.edu/csdb');
					if (!$conn) {
						$e = oci_error();
						trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR); 
					}
						$comicid = str_replace(' ', '', mysql_real_escape_string(htmlentities($_POST['comicid'])));
						$title = mysql_real_escape_string(htmlentities($_POST['title']));
						$authorlast = mysql_real_escape_string(htmlentities($_POST['authorlast']));
						$authorfirst = mysql_real_escape_string(htmlentities($_POST['authorfirst']));
						$excessionnumber = mysql_real_escape_string(htmlentities($_POST['excessionnumber']));
						$pagenumber = mysql_real_escape_string(htmlentities($_POST['pagenumber']));
						$comictype = mysql_real_escape_string(htmlentities($_POST['comictype']));
						
						if(isset( $_FILES['photourl']['name'])){
						$allowed_filetypes = array('.jpg','.jpeg','.png','.gif');
						$max_filesize = 10485760;
						$upload_path = '../photos/';

						$filename1 = $_FILES['photourl']['name'];
						$photourl = "./photos/" . $filename1;
						$ext = substr($filename1, strpos($filename1,'.'), strlen($filename1)-1);

						if(filesize($_FILES['photourl']['tmp_name']) > $max_filesize)
						  die('The file you attempted to upload is too large.');

						if(!is_writable($upload_path))
						  die('You cannot upload to the specified directory, please CHMOD it to 777.');

						if(move_uploaded_file($_FILES['photourl']['tmp_name'],$upload_path . $filename1)) {
						echo 'Your file upload was successful!';
						}
						else{
							echo "file upload failed";
						}
						}
						if($photourl == "./photos/")
						{
							$photourl = null;
						}
						$statement = "
												SELECT * FROM newspaper WHERE excession_number = '$excessionnumber'
											";
						$stid = oci_parse($conn, $statement);
						oci_execute($stid);
						while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
							$newspaperid = $row['NEWSPAPER_ID'];
						}
						$statement = "
												SELECT * FROM page WHERE newspaper_id = '$newspaperid' AND page_number = '$pagenumber'
											";
						$stid = oci_parse($conn, $statement);
						oci_execute($stid);
						$pageid = null;
						while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
							$pageid = $row['PAGE_ID'];
						}
						if($pageid == null){
							echo "<br/><br/><br/><h1> Error : You selected a page and excession number that were incorrect! <br/> Try again with an excession and page number that exists!</h1>";
							echo "<br/><a href=\"editcomic.php\" class=\"btn btn-primary btn-large\">Reset</a>";
							exit();
						}
						
						$statement="
										UPDATE comic
										SET  title = '$title',
												author_last = '$authorlast',
												author_first = '$authorfirst',
												photo_url = '$photourl',
												page_id = '$pageid',
												comic_type = '$comictype'
										WHERE comic_id = '$comicid'
										";
						$stid = oci_parse($conn, $statement);
						oci_execute($stid);
						echo "
						
							<script type=\"text/javascript\">
								<!--
									window.location = \"tagcomiclink.php?comicid=$comicid&menu=1\"
									//-->
								</script>						
								";

						

				?>
				<a href="editcomic.php">Reset</a>
				<?php
				
				}
			}
		//}			
			else {
				?>
				<h1>Edit Comics</h1>
				<div class="box">
				  <div class="container-1" style="5px">
					  <form method="POST" action="editcomic.php?menu=1">
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
						<input type="search" name="search" id="search" placeholder="Search..." />
						 <select name="selection">
									<option value="comic.title">Title</option>
									<option value="comic.author_first">Author First Name</option>
									<option value="comic.author_last">Author Last Name</option>
									
						</select>
						&nbsp;
						<input type="submit" name="commit" value="Search">
					  </form>
					  <div style="padding-left:20px"
						<a href="../edit.php">Back</a>
					  </div>
				  </div>
				</div>
                  				
                  </div><!-- /col-lg-9 END SECTION MIDDLE -->                   
              </div><!--/row -->
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2014 - Team Venture
              <a href="index.php#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../js/jquery.js"></script>
    <script src="../js/jquery-1.8.3.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../js/jquery.scrollTo.min.js"></script>
    <script src="../js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="../js/common-scripts.js"></script>
    
    <script type="text/javascript" src="../js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="../js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="../js/sparkline-chart.js"></script>    
	<script src="../js/zabuto_calendar.js"></script>	
	

	
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
  </body>
</html>
	<?php
	}
?>